function sqlStr (str) {
    str = str || '';
    return '\'' + str.replace('\'', '\'\'') + '\'';
}

module.exports = sqlStr;