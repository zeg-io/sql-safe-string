# SQL Safe String

Minimal code:

```
var safe = require(‘@zetta/sql-safe-string’);
var email = 'email@domain.com’;
var firstName = ‘Bob'
…
sql = ’SELECT * FROM Table WHERE email=‘ + safe(emailVar) +
      ‘ AND firstName=‘ + safe(firstNameVar);

```

Would result in:
```
SELECT * FROM Table WHERE email=‘email@domain.com’ AND fristName=‘Bob'
```

`O'Ridley` -> `'O’'Ridley'`

`anything' OR 1=1 --` -> `'anything'' OR 1=1 --'` (preventing modification of the code)